#!/usr/bin/python3.7
"""
This paraview plugin enables displaying lidar + video data in PandaSet-like
format by using PandaSet DevKit API
More info on PandaSet and PandaSet DevKit here:
- Dataset description: https://pandaset.org/
- DevKit: https://github.com/scaleapi/pandaset-devkit
- API documentation: https://scaleapi.github.io/pandaset-devkit/index.html

The reader enables displaying:
- point data from both lidars (spinning lidar + forward-facing lidar)
- point-wise and cuboids annotations
- video streams (Not returned by default, but setting the corresponding
parameter after loading the sequence enables returning either a specific one or
all at once)
- [TODO] add gps positions

Prerequisites
-------------
In order to use this plugin, you need:
* paraview >= 5.6
    ** with the following flags activated: Module_vtkFiltersVerdict
    ** using system python >= 3.6
* python 3.6 with pandaset devkit installed

or
* lidarview:
    ** based on paraview 5.6
    ** using system python 3.7
* python 3.7 with pandaset devkit installed

Usage
-----
See filters class documentations.

"""

import os
import sys

from vtkmodules.util.vtkAlgorithm import VTKPythonAlgorithmBase
from vtkmodules.vtkCommonDataModel import vtkPolyData, vtkDataSet, \
                                          vtkDataObject, \
                                          vtkMultiBlockDataSet, vtkImageData
from vtkmodules.vtkFiltersSources import vtkOutlineSource

# vtk-numpy integration
from vtk.numpy_interface import algorithms as algs
from vtk.numpy_interface import dataset_adapter as dsa
import numpy as np

import vtk

from paraview.util.vtkAlgorithm import smproxy, smproperty

import pandaset


def add_vertex_cells_to_point_cloud(obj):
    """add_vertex_cells_to_point_cloud

    Add vertex cells to a vtkPointData
    This is typically used to add cells to a point cloud in order to make it
    visible in paraview
    Args:
        obj: vtkPolyData object to add cells to
    """
    num_pts = obj.GetNumberOfPoints()
    pt_ids = vtk.vtkIdList()
    pt_ids.SetNumberOfIds(num_pts)
    for a in np.arange(num_pts):
        pt_ids.SetId(a, a)

    obj.Allocate(1)
    obj.InsertNextCell(vtk.VTK_POLY_VERTEX, pt_ids)


def bbox_corners_to_vtk_outline_corners(box):
    """bbox_corners_to_vtk_outline_corners.

    Convert bbox corners from pandaset convention to vtkOutlineSource
    convention by reordering the box points like the following:

        7 -------- 4                    6 -------- 7
       /|         /|                   /|         /|
      6 -------- 5 .        --->      4 -------- 5 .
      | |        | |                  | |        | |
      . 3 -------- 0                  . 2 -------- 3
      |/         |/                   |/         |/
      2 -------- 1                    0 -------- 1

    Args:
        box: 8 x n np.array containing coordinates for each point
             (with n the number of coordinates for each point)

    """
    out_box = box[[2, 1, 3, 0, 6, 5, 7, 4], :]

    return out_box


def create_bbox_3d(center_box):
    """create_bbox_3d.

    Create a vtkPolyData object for a box described like the following
    box = [x_center, y_center, z_center, dim_x, dim_y, dim_z, yaw]
    with:
        - x_center, y_center, z_center: Position of the cuboid expressed as the
        center of the cuboid.
        - dim_x, dim_y, dim_z:  Dimensions of the cuboid based on the world
        dimensions. Width of the cuboid is from left to right, length from
        front to back, height from top to bottom.
        - yaw: Rotation of cuboid around the z-axis in radians
        0 radians is equivalent to the direction of the vector `(0, 1, 0)`.
        The vector points at the length-side. Rotation happens
        counter-clockwise, i.e., PI/2 is pointing in the same direction as the
    Args:
        center_box: list containing the coordinates for one box

    """
    corner_box = pandaset.geometry.center_box_to_corners(center_box)
    corner_box = bbox_corners_to_vtk_outline_corners(corner_box)
    box_source = vtkOutlineSource()
    box_source.SetBoxTypeToOriented()
    box_source.SetCorners(corner_box.flatten())
    box_source.Update()

    return box_source.GetOutput()


def add_string_field_data(dataset, value, array_name):
    """add_string_field_data.

    Add a vtkStringArray FieldData named `array_name` to the vtkPolyData
    `dataset` with a single value `value`
    Args:
        dataset: vtkPolyData to add the FieldData to
        value: str, content of the vtkStringArray to create
        array_name: str, name of the vtkStringArray to create
    """
    a = vtk.vtkStringArray()
    a.InsertNextValue(str(value))
    a.SetName(array_name)
    dataset.GetFieldData().AddArray(a)


def create_image_from_pil(pil_img):
    """create_image_from_pil.

    Create a 2D vtkImageData from a PIL image object (see pillow module
    documentation for more info)

    Args:
        pil_img: PIL image
    """
    vtk_img = vtkImageData()
    vtk_img = dsa.WrapDataObject(vtk_img)
    np_img = np.array(pil_img)
    # flip array along vertical axis to fit vtk structure
    np_img = np.flipud(np_img)
    h, w, c = np_img.shape
    vtk_img.SetDimensions(w, h, 1)
    vtk_img.PointData.append(np_img.reshape((-1, c)), "scalars")
    vtk_img.PointData.SetActiveScalars("scalars")

    return vtk_img


NB_OUTPUT_PORTS = 3


@smproxy.source(name="PandasetReader",
                label="Pandaset Reader",
                file_description="PandaSet Dataset Folder",
                documentation="""
                Reader for pandaset-like datasets.\n
                This reader enables visualizing a specific sequence of a
                pandaset-like dataset, including:\n
                - lidar data\n
                - point-wise and cuboids annotations\n
                - video streams\n

                PLease refer to the different properties documentation to
                figure out how to set-up the reader.
                """)
@smproperty.xml("""
    <OutputPort name="LidarFrame"   index="0"   id="port0"/>
    <OutputPort name="Cuboids"      index="1"   id="port1"/>
    <OutputPort name="Cameras"      index="2"   id="port2"/>
    """)
class PandasetReader(VTKPythonAlgorithmBase):
    """PandasetReader.
    Read and display data for a sequence of a Pandaset-like dataset

    Usage:
    See documentation in the xml chunk corresponding to this source.
    """
    def __init__(self):
        VTKPythonAlgorithmBase.__init__(self,
                                        nInputPorts=0,
                                        nOutputPorts=NB_OUTPUT_PORTS,
                                        outputType=['vtkPolyData',
                                                    'vtkMultiBlockDataSet',
                                                    'vtkMultiBlockDataSet'])
        self.dataset_path = ''
        self.sequence_name = ''
        self.dataset = None
        self.sequence = None
        self.selected_camera = None

    def FillOutputPortInformation(self, port, info):
        if port == 0:  # lidar output
            info.Set(vtkDataObject.DATA_TYPE_NAME(), "vtkPolyData")
        elif port == 1:  # cuboids output
            info.Set(vtkDataObject.DATA_TYPE_NAME(), "vtkMultiBlockDataSet")
        elif port == 2:  # cameras output
            info.Set(vtkDataObject.DATA_TYPE_NAME(), "vtkMultiBlockDataSet")

        return 1

    def _get_lidar_frame(self, idx, vtk_output):
        lidar = self.sequence.lidar[idx]
        coordinates = algs.make_vector(lidar['x'].to_numpy(),
                                       lidar['y'].to_numpy(),
                                       lidar['z'].to_numpy())
        # add points
        vtk_output.Points = coordinates
        add_vertex_cells_to_point_cloud(vtk_output)

        # add points attributes as PointData
        vtk_output.PointData.append(lidar['i'].to_numpy(), 'intensity')
        vtk_output.PointData.append(lidar['d'].to_numpy(), 'device')
        vtk_output.PointData.append(lidar['t'].to_numpy(), 'timestamp')

    def _get_cuboids(self, idx, vtk_output):
        cuboids = self.sequence.cuboids[idx]
        nb_cuboids = len(cuboids)
        vtk_output.SetNumberOfBlocks(nb_cuboids)
        for ii, row in cuboids.iterrows():
            center_box = [row["position.x"],
                          row["position.y"],
                          row["position.z"],
                          row["dimensions.x"],
                          row["dimensions.y"],
                          row["dimensions.z"],
                          row["yaw"]]
            box = dsa.WrapDataObject(create_bbox_3d(center_box))
            # Mandatory keys
            add_string_field_data(box, row["label"], "label")
            for key in ["stationary", "camera_used", "cuboids.sensor_id"]:
                box.FieldData.append(row[key], key)

            # Optional keys
            for key in ["attributes.object_motion",
                        "cuboids.sibling_id",
                        "attributes.pedestrian_behavior",
                        "attributes.pedestrian_age",
                        "attributes.rider_status"]:
                if key in cuboids.keys():
                    add_string_field_data(box, row[key], key)

            vtk_output.SetBlock(ii, box.VTKObject)

    def _add_semseg_infos(self, idx, vtk_output):
        if self.sequence.semseg is not None:
            semseg = self.sequence.semseg[idx]
            classes = self.sequence.semseg.classes

            semseg_array = semseg['class'].to_numpy()
            vtk_output.PointData.append(semseg_array, "semseg_class")

            # Add categories as a string array
            # no dataset adaptor utility is implemented for such arrays yet
            categories_vtk_array = vtk.vtkStringArray()
            for class_idx in semseg_array:
                categories_vtk_array.InsertNextValue(
                    classes.get(str(class_idx), "Unknown")
                )

            categories_vtk_array.SetName("semseg_label")
            vtk_output.GetPointData().AddArray(categories_vtk_array)

    def _add_single_camera_frame(self, frame_idx, camera_name,
                                 vtk_multiblock, block_idx):
        camera = self.sequence.camera[camera_name]
        frame = create_image_from_pil(camera[frame_idx])
        add_string_field_data(frame, camera_name, 'camera_name')

        vtk_multiblock.SetBlock(block_idx, frame.VTKObject)

    def _get_all_cameras_frames(self, idx, vtk_output):
        if self.selected_camera.startswith("None"):
            return
        if self.selected_camera.startswith("All"):
            camera_names = self.sequence.camera.keys()
        else:
            camera_names = [self.selected_camera]

        vtk_output.SetNumberOfBlocks(len(camera_names))
        for block_idx, camera_name in enumerate(camera_names):
            self._add_single_camera_frame(idx, camera_name, vtk_output,
                                          block_idx)

    def _get_timesteps(self):
        if self.sequence is not None:
            timesteps = self.sequence.timestamps.data
            return timesteps

        else:
            return None

    def _get_update_time(self, outInfo):
        """_get_update_time.

        Adapted from:
        https://github.com/Kitware/ParaView/blob/master/Examples/Plugins/PythonAlgorithm/PythonAlgorithmExamples.py
        """
        executive = self.GetExecutive()
        timesteps = self._get_timesteps()
        if timesteps is None or len(timesteps) == 0:
            return None

        if outInfo.Has(executive.UPDATE_TIME_STEP()) and len(timesteps) > 0:
            utime = outInfo.Get(executive.UPDATE_TIME_STEP())
            dtime = timesteps[0]
            for atime in timesteps:
                if atime > utime:
                    return dtime
                else:
                    dtime = atime
            return dtime

        assert len(timesteps) > 0
        return timesteps[0]

    @smproperty.xml("""
        <StringVectorProperty name="SequenceRootFolder"
            number_of_elements="1"
            default_values=""
            command="SetSequenceRoot">
            <FileListDomain name='files'/>
            <Hints>
                <UseDirectoryName/>
            </Hints>
            <Documentation>
                Provide the root folder of the sequence to display.
                It should be a direct child of the dataset root.
                e.g "/path/to/dataset/001/"
            </Documentation>
        </StringVectorProperty>
        """)
    def SetSequenceRoot(self, path):
        """SetSequenceRoot.

        Set the root folder of the sequence to display.
        It should be a direct child of the dataset root.
        e.g "/path/to/dataset/001/"

        Args:
            path: str, full path to the sequence
        """
        if path:
            dataset_path, sequence_name = os.path.split(path)
            if dataset_path != self.dataset_path:
                self.dataset = pandaset.DataSet(dataset_path)

            if sequence_name != self.sequence_name:
                self.dataset.unload(self.sequence_name)

            self.sequence = self.dataset[sequence_name]
            print("Loading the data for the whole sequence " +
                  "(this may take some time)...")
            self.sequence.load_timestamps()
            self.sequence.load_lidar()
            self.sequence.load_cuboids()
            self.sequence.load_semseg()
            self.sequence_name = sequence_name
            self.dataset_path = dataset_path

            self.Modified()

    @smproperty.xml("""
        <StringVectorProperty name="AllCamerasList"
                              command="GetCamerasList"
                              information_only="1"
                              si_class="vtkSIDataArrayProperty" />
    """)
    def GetCamerasList(self):
        """GetCamerasList.

        Get the list of all available cameras for the given sequence and add
        None / All keys for specific treatment (see. doc for `SetActiveCamera
        for more details)
        """
        cam_list = vtk.vtkStringArray()
        cam_list.InsertNextValue("None (faster computation)")
        cam_list.InsertNextValue("All (one per block)")
        if self.sequence:
            for camera_name in self.sequence.camera.keys():
                cam_list.InsertNextValue(camera_name)

        return cam_list

    @smproperty.xml("""
        <StringVectorProperty command="SetActiveCamera"
                              information_property="AllCamerasList"
                              label="Camera Output"
                              name="CameraOutput"
                              number_of_elements="1">
            <StringListDomain name="camera_choice">
                <RequiredProperties>
                    <Property name="AllCamerasList"
                              function="StringList"/>
                </RequiredProperties>
            </StringListDomain>
            <Documentation>
                Choose which camera(s) data the source will return.
                Either choose:\n
                - one camera by selecting its name\n
                - all cameras: the `Cameras` output will contain a block for
                each camera.\n
                The user will have to use an `ExtractBlock` filter if they want
                to visualize the data for a specific camera\n
                - None: the `Cameras` output will be empty (this enables faster
                computation as cameras data will not be loaded)\n
                Default is None.
            </Documentation>
        </StringVectorProperty>
        """)
    def SetActiveCamera(self, cam):
        """SetActiveCamera.

        See documentation in the xml chunk corresponding to this property.
        Args:
            cam:
        """
        self.selected_camera = cam
        print("Selected camera:", cam)
        if cam.startswith("None"):
            pass
        elif cam.startswith("All"):
            self.sequence.load_camera()
        elif cam in self.sequence.camera.keys():
            self.sequence.camera[cam].load()
        else:
            print("Unrecognized camera, selecting 'None' instead")
            self.selected_camera = "None"

        self.Modified()

    @smproperty.doublevector(name="TimestepValues",
                             information_only="1",
                             si_class="vtkSITimeStepsProperty")
    def GetTimestepValues(self):
        return self._get_timesteps()

    def RequestData(self, request, inInfo, outInfo):
        data_time = self._get_update_time(outInfo.GetInformationObject(0))
        if data_time is None:
            print("Could not extract timesteps from the current sequence " +
                  "folder. Please select a valid Sequence Root Folder.")

        else:
            data_idx = self._get_timesteps().index(data_time)

            lidar_output = dsa.WrapDataObject(vtkDataSet.GetData(outInfo, 0))
            boxes_output = dsa.WrapDataObject(
                    vtkMultiBlockDataSet.GetData(outInfo, 1)
            )
            cam_output = dsa.WrapDataObject(
                    vtkMultiBlockDataSet.GetData(outInfo, 2)
            )
            self._get_lidar_frame(data_idx, lidar_output)
            self._get_cuboids(data_idx, boxes_output)
            self._add_semseg_infos(data_idx, lidar_output)
            self._get_all_cameras_frames(data_idx, cam_output)

            lidar_output.GetInformation().Set(lidar_output.DATA_TIME_STEP(),
                                              data_time)
            boxes_output.GetInformation().Set(boxes_output.DATA_TIME_STEP(),
                                              data_time)
            cam_output.GetInformation().Set(cam_output.DATA_TIME_STEP(),
                                            data_time)

        return 1

    def RequestInformation(self, request, in_info, out_info):
        timesteps = self._get_timesteps()

        executive = self.GetExecutive()
        out_info_objects = [out_info.GetInformationObject(ii)
                            for ii in range(NB_OUTPUT_PORTS)]

        for info in out_info_objects:
            info.Remove(executive.TIME_STEPS())
            info.Remove(executive.TIME_RANGE())

            if timesteps is not None:
                for t in timesteps:
                    info.Append(executive.TIME_STEPS(), t)

                info.Append(executive.TIME_RANGE(), timesteps[0])
                info.Append(executive.TIME_RANGE(), timesteps[-1])

        return 1

