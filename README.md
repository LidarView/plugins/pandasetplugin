# Pandaset Plugin

This paraview plugin enables displaying lidar + video data in PandaSet-like
format by using PandaSet DevKit API
More info on PandaSet and PandaSet DevKit here:
- Dataset description: https://pandaset.org/
- DevKit: https://github.com/scaleapi/pandaset-devkit
- API documentation: https://scaleapi.github.io/pandaset-devkit/index.html

Here is an example of what can be displayed using this reader:
* on the left, the point cloud colored by semantic category annotation and
bounding box annotations colored with moving object status
* on the right, the view from 3 of the 6 cameras


![alt text](reader_example.png "Reader example")

## Prerequisites

In order to use this plugin, you need:
* paraview >= 5.6
    ** with the following flags activated: Module_vtkFiltersVerdict
    ** using system python >= 3.6
* python 3.6 with pandaset devkit installed

or
* lidarview:
    ** based on paraview 5.6
    ** using system python 3.7
* python 3.7 with pandaset devkit installed


## Usage

In order to enable this plugin in a Paraview-based application:
* Open Paraview/Lidarview
* If using Paraview: navigate to `Tools` > `Manage Plugins...`
* If using Lidarview: Activate the advanced features and navigate to `Advanced`
> `Tools` > `Manage Plugins...`
* Load the python file corresponding to the plugin you want to use (eg.
`PandasetReader.py`)
* Select the corresponding source/filter among the usual sources/filters (e.g.
in the `Sources` menu)
* Set the source/filter parameters in the `Properties` panel


## Content

### PandasetReader

#### Overview
The reader enables displaying:
- point data from both lidars
- point-wise and cuboids annotations
- video streams (Not returned by default, but setting the corresponding
parameter after loading the sequence enables returning either a specific one or
all at once)
- [TODO] add gps positions

#### Usage

Please refer to the docstrings in the Pandaset reader python file.
